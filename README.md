# GlyCosmosにおけるWURCSRDF&SPARQL

## SPARQLQuery

## 依存関係
* https://gitlab.com/glycosmos/dockerimage/wurcsrdf-api/-/blob/master/app/build.gradle

```
repositories {
    maven {
        url "https://glycoinfo.gitlab.io/wurcsframework"
    }
    maven {
        url "https://nexus.glycoinfo.org/content/repositories/releases"
    }
    mavenCentral()
}

dependencies {
    implementation 'org.glycoinfo.WURCSFramework:WURCSRDF:1.2.6'


```
## WURCSRDF

* setting: https://gitlab.com/glycoinfo/wurcsrdf/-/blob/master/pom.xml

# WURCSRDFを生成し、追加するバッチ

* repository: https://gitlab.com/glycosmos/dockerimage/wurcsrdf-batch

* code: https://gitlab.com/glycosmos/dockerimage/wurcsrdf-batch/-/blob/main/app/src/main/java/batch/App.java

```java
package batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.util.array.WURCSExporter;
import java.util.TreeSet;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModelGlycan051;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSSequence2ExporterRDFModel;
import java.util.LinkedList;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModelMS;
import org.glycoinfo.WURCSFramework.util.WURCSException;

import java.io.IOException;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.net.URLEncoder;

public class App {
    public String wurcs2wurcsrdf(String acc, String wurcs) {
        try{
            WURCSFactory fac = new WURCSFactory(wurcs);
            WURCSArray wArr = fac.getArray();
            WURCSExporter t_oExport = new WURCSExporter();
            TreeSet<String> mss = new TreeSet<String>();
            for(UniqueRES t_oURES: wArr.getUniqueRESs()){
                String t_strMS = t_oExport.getUniqueRESString(t_oURES);
                if(!mss.contains(t_strMS)){
                    mss.add(t_strMS);
                }
            }
            WURCSRDFModelGlycan051 mg = new WURCSRDFModelGlycan051(acc, wArr, false);
            String rdf1 = mg.get_RDF("TURTLE");
            WURCSSequence2 Seq2 = fac.getSequence();
            WURCSSequence2ExporterRDFModel rdfm = new WURCSSequence2ExporterRDFModel(acc, Seq2, false);
            String rdf2 = rdfm.get_RDF("TURTLE");
            LinkedList<MS> msList = new LinkedList<MS>();
            WURCSImporter t_oImport = new WURCSImporter();
            for(String ms: mss){
                msList.add(t_oImport.extractMS(ms));
            }
            WURCSRDFModelMS mSExpo = new WURCSRDFModelMS(msList, false);
            String rdf3 = mSExpo.get_RDF("TURTLE");
            return rdf1 + rdf2 + rdf3;
        }catch(WURCSException e){
            e.printStackTrace();
            return e.toString();
        }
    }
```



# SPARQL生成


* repository: https://gitlab.com/glycosmos/dockerimage/wurcsrdf-api

* code: https://gitlab.com/glycosmos/dockerimage/wurcsrdf-api/-/blob/master/app/src/main/java/api/App.java


```java
    public String wurcs2sparql(String graph, String wurcs, boolean supersumption, boolean rootnode) {
        WURCSSequence2ExporterSPARQL t_oExport = new WURCSSequence2ExporterSPARQL();
        t_oExport.addTargetGraphURI(graph);
        t_oExport.setMSGraphURI(graph);
        t_oExport.setSearchSupersumption(supersumption);
        t_oExport.setSpecifyRootNode(rootnode);
        try{
            WURCSImporter t_oImport = new WURCSImporter();
            WURCSArray t_oWURCS = t_oImport.extractWURCSArray(wurcs);
            WURCSArrayToSequence2 t_oA2S = new WURCSArrayToSequence2();
            t_oA2S.start(t_oWURCS);
            WURCSSequence2 t_oSeq = t_oA2S.getSequence();
            t_oExport.start(t_oSeq);
            String t_strSPARQL = t_oExport.getQuery();
            return t_strSPARQL;
        }catch(WURCSFormatException e){
            e.printStackTrace();
            return e.toString();
        }
    }

    public String wurcs2wurcsrdf(String acc, String wurcs) {
        try{
            WURCSFactory fac = new WURCSFactory(wurcs);
            WURCSArray wArr = fac.getArray();
            WURCSExporter t_oExport = new WURCSExporter();
            TreeSet<String> mss = new TreeSet<String>();
            for(UniqueRES t_oURES: wArr.getUniqueRESs()){
                String t_strMS = t_oExport.getUniqueRESString(t_oURES);
                if(!mss.contains(t_strMS)){
                    mss.add(t_strMS);
                }
            }
            WURCSRDFModelGlycan051 mg = new WURCSRDFModelGlycan051(acc, wArr, false);
            String rdf1 = mg.get_RDF("TURTLE");
            WURCSSequence2 Seq2 = fac.getSequence();
            WURCSSequence2ExporterRDFModel rdfm = new WURCSSequence2ExporterRDFModel(acc, Seq2, false);
            String rdf2 = rdfm.get_RDF("TURTLE");
            LinkedList<MS> msList = new LinkedList<MS>();
            WURCSImporter t_oImport = new WURCSImporter();
            for(String ms: mss){
                msList.add(t_oImport.extractMS(ms));
            }
            WURCSRDFModelMS mSExpo = new WURCSRDFModelMS(msList, false);
            String rdf3 = mSExpo.get_RDF("TURTLE");
            return rdf1 + rdf2 + rdf3;
        }catch(WURCSException e){
            e.printStackTrace();
            return e.toString();
        }
    }

```
